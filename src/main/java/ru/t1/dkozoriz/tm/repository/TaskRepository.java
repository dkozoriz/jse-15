package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.api.repository.ITaskRepository;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>(
            Arrays.asList(
                    new Task("Task 1", Status.COMPLETED),
                    new Task("Task 2", Status.IN_PROGRESS),
                    new Task("Task 3")
            )
    );

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    public List<Task> findAll(final Comparator<? super IWBS> comparator) {
        final List<Task> result = new ArrayList<>(tasks);
        result.sort(comparator);
        return result;
    }

    public List<Task> findAllByProjectId(final String ProjectId) {
        List<Task> projectTasks = new ArrayList<>();
        for (final Task task : tasks) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(ProjectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

    public Task findOneById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    ;

    public int getSize() {
        return tasks.size();
    }

    public void remove(final Task task) {
        if (task == null) return;
        tasks.remove(task);
    }

    public Task removeById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    public Task removeByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

}